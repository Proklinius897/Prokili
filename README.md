BOTCONNECT V1.0
Requis : node.js, npm
Tout d'abord lancer un npm install dans le dossier botconnect
Exemple d'utilisation : Pour lancer une simulation, il fait d'abord télécharger les données : on exécutera ./botconnect.sh backfill  --days 
ici le "selector" est le nom des deux monnaies échangées et le site sur lequel on va récupérer les données.
Par exemple, si je veux télécharger l'historique de l'Ethereum par rapport au dollar sur le site Poloniex et sur 20 jours,
alors on écrira : ./botconnect.sh backfill poloniex.eth-usdt --days 20
une fois les valeurs enregistrées, pour lancer la simulation on utilisera la commande sim :
pour cet exemple, la stratégie par défaut est "trend_ema" (analyse de la moyenne exponentielle variable),
mais si je veux simuler 20 jours de trading avec la stratégie "speed" :
./botconnect.sh sim poloniex.eth-usdt --days 20 --strategy speed
Après les calculs, le résultat sera dans le dossiers /simulations/
sous la forme d'un fichier .html restranscrivant les infos du trade
